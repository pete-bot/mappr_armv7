# CMake generated Testfile for 
# Source directory: /home/pi/mappr/src/nodelet_core/test_nodelet_topic_tools
# Build directory: /home/pi/mappr/build/nodelet_core/test_nodelet_topic_tools
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(_ctest_test_nodelet_topic_tools_rostest_test_test_nodelet_throttle.launch "/home/pi/mappr/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/indigo/share/catkin/cmake/test/run_tests.py" "/home/pi/mappr/build/test_results/test_nodelet_topic_tools/rostest-test_test_nodelet_throttle.xml" "--return-code" "/opt/ros/indigo/share/rostest/cmake/../../../bin/rostest --pkgdir=/home/pi/mappr/src/nodelet_core/test_nodelet_topic_tools --package=test_nodelet_topic_tools --results-filename test_test_nodelet_throttle.xml --results-base-dir \"/home/pi/mappr/build/test_results\" /home/pi/mappr/src/nodelet_core/test_nodelet_topic_tools/test/test_nodelet_throttle.launch ")
