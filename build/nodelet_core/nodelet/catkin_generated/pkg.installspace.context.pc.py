# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/pi/mappr/install/include;/usr/include".split(';') if "/home/pi/mappr/install/include;/usr/include" != "" else []
PROJECT_CATKIN_DEPENDS = "bondcpp;message_runtime;pluginlib;rosconsole;roscpp;std_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lnodeletlib;-l:/usr/lib/arm-linux-gnueabihf/libuuid.so".split(';') if "-lnodeletlib;-l:/usr/lib/arm-linux-gnueabihf/libuuid.so" != "" else []
PROJECT_NAME = "nodelet"
PROJECT_SPACE_DIR = "/home/pi/mappr/install"
PROJECT_VERSION = "1.9.3"
