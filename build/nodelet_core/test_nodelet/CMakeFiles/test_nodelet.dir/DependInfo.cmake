# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/pi/mappr/src/nodelet_core/test_nodelet/src/console_tests.cpp" "/home/pi/mappr/build/nodelet_core/test_nodelet/CMakeFiles/test_nodelet.dir/src/console_tests.cpp.o"
  "/home/pi/mappr/src/nodelet_core/test_nodelet/src/failing_nodelet.cpp" "/home/pi/mappr/build/nodelet_core/test_nodelet/CMakeFiles/test_nodelet.dir/src/failing_nodelet.cpp.o"
  "/home/pi/mappr/src/nodelet_core/test_nodelet/src/plus.cpp" "/home/pi/mappr/build/nodelet_core/test_nodelet/CMakeFiles/test_nodelet.dir/src/plus.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"test_nodelet\""
  "ROSCONSOLE_BACKEND_LOG4CXX"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/pi/mappr/build/nodelet_core/nodelet/CMakeFiles/nodeletlib.dir/DependInfo.cmake"
  "/home/pi/mappr/build/bond_core/bondcpp/CMakeFiles/bondcpp.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/pi/mappr/devel/include"
  "/home/pi/mappr/src/nodelet_core/nodelet/include"
  "/home/pi/mappr/src/bond_core/bondcpp/include"
  "/home/pi/mappr/src/bond_core/smclib/include"
  "/opt/ros/indigo/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
