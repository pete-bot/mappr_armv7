# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/pi/mappr/devel/include;/home/pi/mappr/src/nodelet_core/nodelet_topic_tools/include;/usr/include".split(';') if "/home/pi/mappr/devel/include;/home/pi/mappr/src/nodelet_core/nodelet_topic_tools/include;/usr/include" != "" else []
PROJECT_CATKIN_DEPENDS = "dynamic_reconfigure;message_filters;nodelet;pluginlib;roscpp".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-l:/usr/lib/libboost_thread-mt.so;-lpthread".split(';') if "-l:/usr/lib/libboost_thread-mt.so;-lpthread" != "" else []
PROJECT_NAME = "nodelet_topic_tools"
PROJECT_SPACE_DIR = "/home/pi/mappr/devel"
PROJECT_VERSION = "1.9.3"
