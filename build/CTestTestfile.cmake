# CMake generated Testfile for 
# Source directory: /home/pi/mappr/src
# Build directory: /home/pi/mappr/build
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(bond_core/bond_core)
SUBDIRS(image_common/image_common)
SUBDIRS(joystick_drivers/joystick_drivers)
SUBDIRS(nodelet_core/nodelet_core)
SUBDIRS(rosserial/rosserial)
SUBDIRS(rosserial/rosserial_arduino)
SUBDIRS(rosserial/rosserial_client)
SUBDIRS(rosserial/rosserial_msgs)
SUBDIRS(rosserial/rosserial_python)
SUBDIRS(rosserial/rosserial_xbee)
SUBDIRS(bond_core/smclib)
SUBDIRS(bond_core/bond)
SUBDIRS(image_common/camera_calibration_parsers)
SUBDIRS(bond_core/bondcpp)
SUBDIRS(bond_core/bondpy)
SUBDIRS(nodelet_core/nodelet)
SUBDIRS(joystick_drivers/ps3joy)
SUBDIRS(image_common/image_transport)
SUBDIRS(image_common/camera_info_manager)
SUBDIRS(gscam)
SUBDIRS(image_common/polled_camera)
SUBDIRS(rosserial/rosserial_embeddedlinux)
SUBDIRS(rosserial/rosserial_windows)
SUBDIRS(rplidar_ros)
SUBDIRS(joystick_drivers/spacenav_node)
SUBDIRS(bond_core/test_bond)
SUBDIRS(nodelet_core/test_nodelet)
SUBDIRS(joystick_drivers/joy)
SUBDIRS(nodelet_core/nodelet_topic_tools)
SUBDIRS(rosserial/rosserial_server)
SUBDIRS(nodelet_core/test_nodelet_topic_tools)
SUBDIRS(joystick_drivers/wiimote)
