#!/bin/sh -x

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
    DESTDIR_ARG="--root=$DESTDIR"
fi

cd "/home/pi/mappr/src/joystick_drivers/wiimote"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
/usr/bin/env \
    PYTHONPATH="/home/pi/mappr/install/lib/python2.7/dist-packages:/home/pi/mappr/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/pi/mappr/build" \
    "/usr/bin/python" \
    "/home/pi/mappr/src/joystick_drivers/wiimote/setup.py" \
    build --build-base "/home/pi/mappr/build/joystick_drivers/wiimote" \
    install \
    $DESTDIR_ARG \
    --install-layout=deb --prefix="/home/pi/mappr/install" --install-scripts="/home/pi/mappr/install/bin"
