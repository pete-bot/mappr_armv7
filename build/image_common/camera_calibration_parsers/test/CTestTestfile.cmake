# CMake generated Testfile for 
# Source directory: /home/pi/mappr/src/image_common/camera_calibration_parsers/test
# Build directory: /home/pi/mappr/build/image_common/camera_calibration_parsers/test
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(_ctest_camera_calibration_parsers_nosetests_parser.py "/home/pi/mappr/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/indigo/share/catkin/cmake/test/run_tests.py" "/home/pi/mappr/build/test_results/camera_calibration_parsers/nosetests-parser.py.xml" "--return-code" "/usr/bin/cmake -E make_directory /home/pi/mappr/build/test_results/camera_calibration_parsers" "/usr/bin/nosetests-2.7 -P --process-timeout=60 /home/pi/mappr/src/image_common/camera_calibration_parsers/test/parser.py --with-xunit --xunit-file=/home/pi/mappr/build/test_results/camera_calibration_parsers/nosetests-parser.py.xml")
