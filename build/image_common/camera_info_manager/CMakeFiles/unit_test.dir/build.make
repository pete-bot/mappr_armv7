# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/pi/mappr/src

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/pi/mappr/build

# Include any dependencies generated for this target.
include image_common/camera_info_manager/CMakeFiles/unit_test.dir/depend.make

# Include the progress variables for this target.
include image_common/camera_info_manager/CMakeFiles/unit_test.dir/progress.make

# Include the compile flags for this target's objects.
include image_common/camera_info_manager/CMakeFiles/unit_test.dir/flags.make

image_common/camera_info_manager/CMakeFiles/unit_test.dir/tests/unit_test.cpp.o: image_common/camera_info_manager/CMakeFiles/unit_test.dir/flags.make
image_common/camera_info_manager/CMakeFiles/unit_test.dir/tests/unit_test.cpp.o: /home/pi/mappr/src/image_common/camera_info_manager/tests/unit_test.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/pi/mappr/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object image_common/camera_info_manager/CMakeFiles/unit_test.dir/tests/unit_test.cpp.o"
	cd /home/pi/mappr/build/image_common/camera_info_manager && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/unit_test.dir/tests/unit_test.cpp.o -c /home/pi/mappr/src/image_common/camera_info_manager/tests/unit_test.cpp

image_common/camera_info_manager/CMakeFiles/unit_test.dir/tests/unit_test.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/unit_test.dir/tests/unit_test.cpp.i"
	cd /home/pi/mappr/build/image_common/camera_info_manager && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/pi/mappr/src/image_common/camera_info_manager/tests/unit_test.cpp > CMakeFiles/unit_test.dir/tests/unit_test.cpp.i

image_common/camera_info_manager/CMakeFiles/unit_test.dir/tests/unit_test.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/unit_test.dir/tests/unit_test.cpp.s"
	cd /home/pi/mappr/build/image_common/camera_info_manager && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/pi/mappr/src/image_common/camera_info_manager/tests/unit_test.cpp -o CMakeFiles/unit_test.dir/tests/unit_test.cpp.s

image_common/camera_info_manager/CMakeFiles/unit_test.dir/tests/unit_test.cpp.o.requires:
.PHONY : image_common/camera_info_manager/CMakeFiles/unit_test.dir/tests/unit_test.cpp.o.requires

image_common/camera_info_manager/CMakeFiles/unit_test.dir/tests/unit_test.cpp.o.provides: image_common/camera_info_manager/CMakeFiles/unit_test.dir/tests/unit_test.cpp.o.requires
	$(MAKE) -f image_common/camera_info_manager/CMakeFiles/unit_test.dir/build.make image_common/camera_info_manager/CMakeFiles/unit_test.dir/tests/unit_test.cpp.o.provides.build
.PHONY : image_common/camera_info_manager/CMakeFiles/unit_test.dir/tests/unit_test.cpp.o.provides

image_common/camera_info_manager/CMakeFiles/unit_test.dir/tests/unit_test.cpp.o.provides.build: image_common/camera_info_manager/CMakeFiles/unit_test.dir/tests/unit_test.cpp.o

# Object files for target unit_test
unit_test_OBJECTS = \
"CMakeFiles/unit_test.dir/tests/unit_test.cpp.o"

# External object files for target unit_test
unit_test_EXTERNAL_OBJECTS =

/home/pi/mappr/devel/lib/camera_info_manager/unit_test: image_common/camera_info_manager/CMakeFiles/unit_test.dir/tests/unit_test.cpp.o
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: image_common/camera_info_manager/CMakeFiles/unit_test.dir/build.make
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /home/pi/mappr/devel/lib/libcamera_info_manager.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: gtest/libgtest.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /home/pi/mappr/devel/lib/libcamera_calibration_parsers.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /home/pi/mappr/devel/lib/libimage_transport.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /opt/ros/indigo/lib/libmessage_filters.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /usr/lib/libtinyxml.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /opt/ros/indigo/lib/libclass_loader.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /usr/lib/libPocoFoundation.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /usr/lib/arm-linux-gnueabihf/libdl.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /opt/ros/indigo/lib/libroscpp.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /usr/lib/libboost_signals-mt.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /usr/lib/libboost_filesystem-mt.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /opt/ros/indigo/lib/librosconsole.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /opt/ros/indigo/lib/librosconsole_log4cxx.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /opt/ros/indigo/lib/librosconsole_backend_interface.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /usr/lib/liblog4cxx.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /usr/lib/libboost_regex-mt.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /opt/ros/indigo/lib/libroscpp_serialization.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /opt/ros/indigo/lib/librostime.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /usr/lib/libboost_date_time-mt.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /opt/ros/indigo/lib/libxmlrpcpp.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /opt/ros/indigo/lib/libcpp_common.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /usr/lib/libboost_system-mt.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /usr/lib/libboost_thread-mt.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /usr/lib/arm-linux-gnueabihf/libpthread.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /usr/lib/arm-linux-gnueabihf/libconsole_bridge.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: /opt/ros/indigo/lib/libroslib.so
/home/pi/mappr/devel/lib/camera_info_manager/unit_test: image_common/camera_info_manager/CMakeFiles/unit_test.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable /home/pi/mappr/devel/lib/camera_info_manager/unit_test"
	cd /home/pi/mappr/build/image_common/camera_info_manager && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/unit_test.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
image_common/camera_info_manager/CMakeFiles/unit_test.dir/build: /home/pi/mappr/devel/lib/camera_info_manager/unit_test
.PHONY : image_common/camera_info_manager/CMakeFiles/unit_test.dir/build

image_common/camera_info_manager/CMakeFiles/unit_test.dir/requires: image_common/camera_info_manager/CMakeFiles/unit_test.dir/tests/unit_test.cpp.o.requires
.PHONY : image_common/camera_info_manager/CMakeFiles/unit_test.dir/requires

image_common/camera_info_manager/CMakeFiles/unit_test.dir/clean:
	cd /home/pi/mappr/build/image_common/camera_info_manager && $(CMAKE_COMMAND) -P CMakeFiles/unit_test.dir/cmake_clean.cmake
.PHONY : image_common/camera_info_manager/CMakeFiles/unit_test.dir/clean

image_common/camera_info_manager/CMakeFiles/unit_test.dir/depend:
	cd /home/pi/mappr/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/pi/mappr/src /home/pi/mappr/src/image_common/camera_info_manager /home/pi/mappr/build /home/pi/mappr/build/image_common/camera_info_manager /home/pi/mappr/build/image_common/camera_info_manager/CMakeFiles/unit_test.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : image_common/camera_info_manager/CMakeFiles/unit_test.dir/depend

