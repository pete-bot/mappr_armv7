# CMake generated Testfile for 
# Source directory: /home/pi/mappr/src/image_common/camera_info_manager
# Build directory: /home/pi/mappr/build/image_common/camera_info_manager
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(_ctest_camera_info_manager_rostest_tests_unit_test.test "/home/pi/mappr/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/indigo/share/catkin/cmake/test/run_tests.py" "/home/pi/mappr/build/test_results/camera_info_manager/rostest-tests_unit_test.xml" "--return-code" "/opt/ros/indigo/share/rostest/cmake/../../../bin/rostest --pkgdir=/home/pi/mappr/src/image_common/camera_info_manager --package=camera_info_manager --results-filename tests_unit_test.xml --results-base-dir \"/home/pi/mappr/build/test_results\" /home/pi/mappr/src/image_common/camera_info_manager/tests/unit_test.test ")
