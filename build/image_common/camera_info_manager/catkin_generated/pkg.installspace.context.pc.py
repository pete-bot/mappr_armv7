# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/pi/mappr/install/include;/usr/include".split(';') if "/home/pi/mappr/install/include;/usr/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;sensor_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lcamera_info_manager".split(';') if "-lcamera_info_manager" != "" else []
PROJECT_NAME = "camera_info_manager"
PROJECT_SPACE_DIR = "/home/pi/mappr/install"
PROJECT_VERSION = "1.11.7"
