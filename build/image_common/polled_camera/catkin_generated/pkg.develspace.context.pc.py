# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/pi/mappr/devel/include;/home/pi/mappr/src/image_common/polled_camera/include".split(';') if "/home/pi/mappr/devel/include;/home/pi/mappr/src/image_common/polled_camera/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;sensor_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lpolled_camera".split(';') if "-lpolled_camera" != "" else []
PROJECT_NAME = "polled_camera"
PROJECT_SPACE_DIR = "/home/pi/mappr/devel"
PROJECT_VERSION = "1.11.7"
