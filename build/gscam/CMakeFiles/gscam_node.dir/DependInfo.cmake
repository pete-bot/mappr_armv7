# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/pi/mappr/src/gscam/src/gscam_node.cpp" "/home/pi/mappr/build/gscam/CMakeFiles/gscam_node.dir/src/gscam_node.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"gscam\""
  "ROSCONSOLE_BACKEND_LOG4CXX"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/pi/mappr/build/gscam/CMakeFiles/gscam.dir/DependInfo.cmake"
  "/home/pi/mappr/build/image_common/image_transport/CMakeFiles/image_transport.dir/DependInfo.cmake"
  "/home/pi/mappr/build/nodelet_core/nodelet/CMakeFiles/nodeletlib.dir/DependInfo.cmake"
  "/home/pi/mappr/build/bond_core/bondcpp/CMakeFiles/bondcpp.dir/DependInfo.cmake"
  "/home/pi/mappr/build/image_common/camera_calibration_parsers/CMakeFiles/camera_calibration_parsers.dir/DependInfo.cmake"
  "/home/pi/mappr/build/image_common/camera_info_manager/CMakeFiles/camera_info_manager.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/pi/mappr/src/gscam/include"
  "/home/pi/mappr/devel/include"
  "/home/pi/mappr/src/image_common/image_transport/include"
  "/home/pi/mappr/src/nodelet_core/nodelet/include"
  "/home/pi/mappr/src/bond_core/bondcpp/include"
  "/home/pi/mappr/src/bond_core/smclib/include"
  "/home/pi/mappr/src/image_common/camera_calibration_parsers/include"
  "/home/pi/mappr/src/image_common/camera_info_manager/include"
  "/opt/ros/indigo/include"
  "/usr/include/gstreamer-0.10"
  "/usr/include/glib-2.0"
  "/usr/lib/arm-linux-gnueabihf/glib-2.0/include"
  "/usr/include/libxml2"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
