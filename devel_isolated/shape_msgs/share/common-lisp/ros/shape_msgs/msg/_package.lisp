(cl:defpackage shape_msgs-msg
  (:use )
  (:export
   "<MESH>"
   "MESH"
   "<PLANE>"
   "PLANE"
   "<SOLIDPRIMITIVE>"
   "SOLIDPRIMITIVE"
   "<MESHTRIANGLE>"
   "MESHTRIANGLE"
  ))

