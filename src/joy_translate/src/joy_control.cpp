// program to allow easy control of a robot using an xbox controller


#include <ros/ros.h>

#include <iostream>
#include <sensor_msgs/Joy.h>		// messages from joy - contain xboxdrv content
#include <geometry_msgs/Twist.h>	// messages to output twist message

// need to subscribe to joy topic
// need to publish special twist message - this will be subscribed to by the 

class JoyDriver
{
    ros::NodeHandle nh_;
	ros::Subscriber joy_sub_;
    ros::Publisher joy_pub_;

	public:
		JoyDriver(const ros::NodeHandle& n);
		{
			nh_ = n;
			joy_sub_ = nh.subscribe("/joy", 10, JoyDriver::joyCallBack, this);
			joy_pub_ = nh_.advertise<geometry_msgs::Twist>("joy_twist", 1000);
		}


	private:
		void  joyCallback(const sensor_msgs::Joy::ConstPtr& joy_msg)
		{
			geometry_msgs::Twist vel_msg;
			vel_msg.linear.x = 4*joy_msg.axes[1];
			vel_msg.angular.z = 4*joy_msg.axes[0];
			joy_pub_.publish(vel_msg);
		} 
}

JoyDriver::JoyDriver(const ros::NodeHandle& n);
		{
			nh_ = n;
			joy_sub_ = nh.subscribe("/joy", 10, JoyDriver::joyCallBack, this);
			joy_pub_ = nh_.advertise<geometry_msgs::Twist>("joy_twist", 1000);
		}


int main(int argc, char** argv) {
    ros::init(argc, argv, "joy_control");
    //    ROS_ERROR("In main");
    ros::NodeHandle n;
    JoyDriver jd(n);

    ros::Rate loop_rate(10);

    while (n.ok()) {
        ros::spinOnce();
    }
}



/*
	#!/usr/bin/env python
	import rospy
	from geometry_msgs.msg import Twist
	from sensor_msgs.msg import Joy

	# Author: Andrew Dai
	# This ROS Node converts Joystick inputs from the joy node
	# into commands for turtlesim

	# Receives joystick messages (subscribed to Joy topic)
	# then converts the joysick inputs into Twist commands
	# axis 1 aka left stick vertical controls linear speed
	# axis 0 aka left stick horizonal controls angular speed
	def callback(data):
	    twist = Twist()
	    # vertical left stick axis = linear rate
	    twist.linear.x = 4*data.axes[1]
	    # horizontal left stick axis = turn rate
	    twist.angular.z = 4*data.axes[0]
	    pub.publish(twist)

	# Intializes everything
	def start():
	    # publishing to "turtle1/cmd_vel" to control turtle1
	    global pub
	    pub = rospy.Publisher('turtle1/cmd_vel', Twist)
	    # subscribed to joystick inputs on topic "joy"
	    rospy.Subscriber("joy", Joy, callback)
	    # starts the node
	    rospy.init_node('Joy2Turtle')
	    rospy.spin()

	if __name__ == '__main__':
	    start()
*/