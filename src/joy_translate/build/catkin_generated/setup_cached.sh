#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/pi/mappr/src/joy_translate/build/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/pi/mappr/src/joy_translate/build/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/pi/mappr/src/joy_translate/build/devel/lib:/home/pi/mappr/src/joy_translate/build/devel/lib/arm-linux-gnueabihf:/home/pi/mappr/devel/lib/arm-linux-gnueabihf:/home/pi/catkin_ws/install/lib/arm-linux-gnueabihf:/opt/ros/indigo/lib/arm-linux-gnueabihf:/home/pi/mappr/devel/lib:/home/pi/catkin_ws/install/lib:/opt/ros/indigo/lib"
export PATH="/home/pi/mappr/src/joy_translate/build/devel/bin:$PATH"
export PKG_CONFIG_PATH="/home/pi/mappr/src/joy_translate/build/devel/lib/pkgconfig:/home/pi/mappr/src/joy_translate/build/devel/lib/arm-linux-gnueabihf/pkgconfig:/home/pi/mappr/devel/lib/arm-linux-gnueabihf/pkgconfig:/home/pi/catkin_ws/install/lib/arm-linux-gnueabihf/pkgconfig:/opt/ros/indigo/lib/arm-linux-gnueabihf/pkgconfig:/home/pi/mappr/devel/lib/pkgconfig:/home/pi/catkin_ws/install/lib/pkgconfig:/opt/ros/indigo/lib/pkgconfig"
export PYTHONPATH="/home/pi/mappr/src/joy_translate/build/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROS_PACKAGE_PATH="/home/pi/mappr/src/joy_translate:$ROS_PACKAGE_PATH"