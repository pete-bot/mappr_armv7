# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/pi/mappr/src/joy_translate

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/pi/mappr/src/joy_translate/build

# Include any dependencies generated for this target.
include CMakeFiles/joy_translate.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/joy_translate.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/joy_translate.dir/flags.make

CMakeFiles/joy_translate.dir/src/joy_translate.cpp.o: CMakeFiles/joy_translate.dir/flags.make
CMakeFiles/joy_translate.dir/src/joy_translate.cpp.o: ../src/joy_translate.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/pi/mappr/src/joy_translate/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/joy_translate.dir/src/joy_translate.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/joy_translate.dir/src/joy_translate.cpp.o -c /home/pi/mappr/src/joy_translate/src/joy_translate.cpp

CMakeFiles/joy_translate.dir/src/joy_translate.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/joy_translate.dir/src/joy_translate.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/pi/mappr/src/joy_translate/src/joy_translate.cpp > CMakeFiles/joy_translate.dir/src/joy_translate.cpp.i

CMakeFiles/joy_translate.dir/src/joy_translate.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/joy_translate.dir/src/joy_translate.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/pi/mappr/src/joy_translate/src/joy_translate.cpp -o CMakeFiles/joy_translate.dir/src/joy_translate.cpp.s

CMakeFiles/joy_translate.dir/src/joy_translate.cpp.o.requires:
.PHONY : CMakeFiles/joy_translate.dir/src/joy_translate.cpp.o.requires

CMakeFiles/joy_translate.dir/src/joy_translate.cpp.o.provides: CMakeFiles/joy_translate.dir/src/joy_translate.cpp.o.requires
	$(MAKE) -f CMakeFiles/joy_translate.dir/build.make CMakeFiles/joy_translate.dir/src/joy_translate.cpp.o.provides.build
.PHONY : CMakeFiles/joy_translate.dir/src/joy_translate.cpp.o.provides

CMakeFiles/joy_translate.dir/src/joy_translate.cpp.o.provides.build: CMakeFiles/joy_translate.dir/src/joy_translate.cpp.o

# Object files for target joy_translate
joy_translate_OBJECTS = \
"CMakeFiles/joy_translate.dir/src/joy_translate.cpp.o"

# External object files for target joy_translate
joy_translate_EXTERNAL_OBJECTS =

../bin/joy_translate: CMakeFiles/joy_translate.dir/src/joy_translate.cpp.o
../bin/joy_translate: CMakeFiles/joy_translate.dir/build.make
../bin/joy_translate: /opt/ros/indigo/lib/libroscpp.so
../bin/joy_translate: /usr/lib/libboost_signals-mt.so
../bin/joy_translate: /usr/lib/libboost_filesystem-mt.so
../bin/joy_translate: /opt/ros/indigo/lib/librosconsole.so
../bin/joy_translate: /opt/ros/indigo/lib/librosconsole_log4cxx.so
../bin/joy_translate: /opt/ros/indigo/lib/librosconsole_backend_interface.so
../bin/joy_translate: /usr/lib/liblog4cxx.so
../bin/joy_translate: /usr/lib/libboost_regex-mt.so
../bin/joy_translate: /opt/ros/indigo/lib/libroscpp_serialization.so
../bin/joy_translate: /opt/ros/indigo/lib/librostime.so
../bin/joy_translate: /usr/lib/libboost_date_time-mt.so
../bin/joy_translate: /opt/ros/indigo/lib/libxmlrpcpp.so
../bin/joy_translate: /opt/ros/indigo/lib/libcpp_common.so
../bin/joy_translate: /usr/lib/libboost_system-mt.so
../bin/joy_translate: /usr/lib/libboost_thread-mt.so
../bin/joy_translate: /usr/lib/arm-linux-gnueabihf/libpthread.so
../bin/joy_translate: /usr/lib/arm-linux-gnueabihf/libconsole_bridge.so
../bin/joy_translate: CMakeFiles/joy_translate.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable ../bin/joy_translate"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/joy_translate.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/joy_translate.dir/build: ../bin/joy_translate
.PHONY : CMakeFiles/joy_translate.dir/build

CMakeFiles/joy_translate.dir/requires: CMakeFiles/joy_translate.dir/src/joy_translate.cpp.o.requires
.PHONY : CMakeFiles/joy_translate.dir/requires

CMakeFiles/joy_translate.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/joy_translate.dir/cmake_clean.cmake
.PHONY : CMakeFiles/joy_translate.dir/clean

CMakeFiles/joy_translate.dir/depend:
	cd /home/pi/mappr/src/joy_translate/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/pi/mappr/src/joy_translate /home/pi/mappr/src/joy_translate /home/pi/mappr/src/joy_translate/build /home/pi/mappr/src/joy_translate/build /home/pi/mappr/src/joy_translate/build/CMakeFiles/joy_translate.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/joy_translate.dir/depend

